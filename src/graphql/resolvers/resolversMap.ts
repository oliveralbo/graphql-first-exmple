import { IResolvers } from 'graphql-tools';

const resolvers: IResolvers = {
Query:{
    hello(){
        return "hello world"
    },
    getCharacters(){
        return [
            {
                id:1,
                name:"diego",
                race: 'MARCIANO'
            },
            {
                id: 2,
                name: "chewacca",
                race: 'WOOKIE'
            }
        ]
    }
}
}

export default resolvers