import express from 'express';
import cors from 'cors';
import { ApolloServer, Config, ExpressContext } from "apollo-server-express";
import { schema } from './graphql';



const app = express();
app.use(cors());

const server = new ApolloServer({
    schema,
    introspection: true,
    playground: true,
  
} as Config<ExpressContext>)
server.start().then(res => {
    server.applyMiddleware({ app });
    app.listen({ port: 5000 }, () =>
        console.log('\x1b[34m%s\x1b[0m','Now browse to http://localhost:5000' + server.graphqlPath)
    )
   })